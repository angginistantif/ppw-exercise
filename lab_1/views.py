from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Angginistanti Fairuz Hanun' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,9,25) #TODO Implement this, format (Year, Month, Date)
npm = 1706984524 # TODO Implement this
Hobi = 'menulis'
Deskripsi = '-'
Institusi = 'Universitas Indonesia'




def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'Hobi' : Hobi, 'Deskripsi' : Deskripsi, 'Institusi' : Institusi}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
